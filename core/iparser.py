from core.response import Response
from abc import ABC, abstractmethod


class IParser(ABC):
    @abstractmethod
    def parse(self, data: str) -> Response:
        raise NotImplementedError
