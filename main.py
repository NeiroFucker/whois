import prettytable
from tools.whois import Whois
from tools.tracer import Trace

TIMEOUT = 1
MAX_HOPS = 30


if __name__ == '__main__':
    dst = "google.com"

    table = prettytable.PrettyTable()
    table.field_names = ["Ip", "AS", "Country", "Description"]

    trace = Trace.trace(dst, MAX_HOPS, TIMEOUT)
    for ip, response in Whois.get_response_list(trace).items():
        table.add_row([ip] + response.to_list())
    print(table)
