from __future__ import annotations

from enum import Enum


class RIR(str, Enum):
    ARIN = "arin"
    RIPE = "ripe"
    APNIC = "apnic"
    LACNIC = "lacnic"
    AFRINIC = "afrinic"

    @classmethod
    def to_list(cls) -> list[RIR]:
        return [cls.ARIN, cls.RIPE, cls.APNIC, cls.LACNIC, cls.AFRINIC]
