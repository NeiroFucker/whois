from scapy.all import *
from scapy.layers.inet import IP, UDP

END_TYPE = 3
DEFAULT_PORT = 33434


class Trace:
    @staticmethod
    def trace(destination: str, max_hops: int, timeout: int) -> list[str]:
        ip_list = list()
        ip_dst = socket.gethostbyname(destination)

        for ttl in range(max_hops):
            pack = IP(dst=ip_dst, ttl=ttl) / UDP(dport=DEFAULT_PORT)
            response = sr1(pack, timeout=timeout, verbose=False)

            if response is not None:
                ip_list.append(response.src)
                if response.type == END_TYPE: break
        return ip_list + [ip_dst]
