class Response:
    def __init__(self, a_system: str, desc: str, country: str) -> None:
        self._desc = desc
        self._country = country
        self._a_system = a_system

    def to_list(self) -> list[str]:
        return [self.description, self.country, self.a_system]

    @property
    def is_full(self) -> bool:
        return all([self._desc, self._country, self._a_system])

    @property
    def description(self) -> str:
        return self._desc or "None"

    @property
    def country(self) -> str:
        return self._country or "None"

    @property
    def a_system(self) -> str:
        return self._a_system or "None"
