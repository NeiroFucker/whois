import socket

from core.rir_list import RIR
from core.iparser import IParser
from core.response import Response

from parsers.arin_parser import ArinParser
from parsers.lacnic_parser import LacnicParser
from parsers.raa_parser import RipeApnicAfrinicParser

PORT = 43
HOST = "whois.{0}.net"


class Whois:
    @classmethod
    def get_response(cls, ip: str) -> Response:
        for rir in RIR.to_list():
            conn = socket.create_connection((HOST.format(rir), PORT))
            conn.sendall(f"{ip}\n".encode("utf-8"))

            data = cls._read_connection_data(conn)
            response = cls._get_rir_parser(rir).parse(data)
            if response.is_full: return response
        return Response("", "", "")

    @classmethod
    def get_response_list(cls, ip_list: list[str]) -> dict[str, Response]:
        return {ip: cls.get_response(ip) for ip in ip_list}

    @staticmethod
    def _read_connection_data(conn: socket.socket) -> str:
        data = ""
        while True:
            buffer = conn.recv(1024)
            if len(buffer) == 0: break
            data += buffer.decode("utf-8")
        return data

    @staticmethod
    def _get_rir_parser(rir: RIR) -> IParser:
        match rir:
            case RIR.ARIN: return ArinParser()
            case RIR.LACNIC: return LacnicParser()
            case _: return RipeApnicAfrinicParser()
