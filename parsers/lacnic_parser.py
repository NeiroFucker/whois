from core.iparser import IParser
from core.response import Response


class LacnicParser(IParser):
    def parse(self, data: str) -> Response:
        desc, country, a_system = "", "", ""
        for line in data.splitlines():
            match line.split(":"):
                case ["aut-num", desc_raw] if desc_raw.strip() != "": desc = desc_raw.strip()
                case ["country", country_raw] if country_raw.strip() != "": country = country_raw.strip()
                case ["owner", a_system_raw] if a_system_raw.strip() != "": a_system = a_system_raw.strip()
        return Response(a_system, desc, country)
